/* eslint-disable no-magic-numbers */
/* eslint-disable default-param-last */
import { AxiosError } from 'axios';

import { setError } from '../../App/App.reducer';
import { AppThunkType } from '../../App/store';
import { authAPI, LoginType, profileAPI } from '../../shared/api/api';

type StateType = {
    isAuth: boolean;
    userId: null | number;
    avaLink: null | string;
    captchaUrl: null | string;
};

const initialState: StateType = {
    isAuth: false,
    userId: null,
    avaLink: null,
    captchaUrl: null,
};

// eslint-disable-next-line prettier/prettier
const authReducer = (state = initialState, action: ToggleIsAuthType| SetUserIdType | SetAvaLinkType | GetCaptchaSuccess): StateType => {
    switch (action.type) {
        case 'LOGIN/TOGGLE_IS_AUTH': {
            return {
                ...state,
                isAuth: action.isAuth,
            };
        }

        case 'LOGIN/SET_USER_ID': {
            return {
                ...state,
                userId: action.id,
            };
        }

        case 'LOGIN/SET_AVATAR_LINK': {
            return {
                ...state,
                avaLink: action.link,
            };
        }

        case 'LOGIN/SET_CAPTCHA': {
            return {
                ...state,
                captchaUrl: action.url,
            };
        }

        default:
            return state;
    }
};

export default authReducer;

type ToggleIsAuthType = {
    type: 'LOGIN/TOGGLE_IS_AUTH';
    isAuth: boolean;
};

const toggleIsAuth = (isAuth: boolean): ToggleIsAuthType => ({
    type: 'LOGIN/TOGGLE_IS_AUTH' as const,
    isAuth,
});

type SetUserIdType = {
    type: 'LOGIN/SET_USER_ID';
    id: number | null;
};

const setUserId = (id: number | null): SetUserIdType => ({
    type: 'LOGIN/SET_USER_ID' as const,
    id,
});

type SetAvaLinkType = {
    type: 'LOGIN/SET_AVATAR_LINK';
    link: string | null;
};

export const setAvaLinkAC = (link: string | null): SetAvaLinkType => ({
    type: 'LOGIN/SET_AVATAR_LINK',
    link,
});

type GetCaptchaSuccess = {
    type: 'LOGIN/SET_CAPTCHA';
    url: string | null;
};

export const getCaptchaSuccess = (url: string | null): GetCaptchaSuccess => ({
    type: 'LOGIN/SET_CAPTCHA',
    url,
});

export const authentication = (): AppThunkType => {
    return async dispatch => {
        try {
            const res = await authAPI.authMe();

            if (res.data.resultCode === 0) {
                dispatch(toggleIsAuth(true));
                dispatch(setUserId(res.data.data.id));
                profileAPI.getUserProfileAPI(res.data.data.id).then(res => {
                    dispatch(setAvaLinkAC(res.data.photos.small));
                });
            } else {
                dispatch(setError(res.data.messages[0]));
                dispatch(toggleIsAuth(false));
                dispatch(setUserId(null));
                dispatch(setAvaLinkAC(null));
            }
        } catch (error) {
            if (error instanceof AxiosError) {
                dispatch(setError(error.message));
            } else {
                dispatch(setError('Unexpected error...'));
            }
        }
    };
};

export const loginization = (data: LoginType): AppThunkType => {
    return async dispatch => {
        try {
            const res = await authAPI.login(data);

            if (res.data.messages[0]) {
                dispatch(setError(res.data.messages[0]));
            }

            if (res.data.resultCode === 0) {
                dispatch(toggleIsAuth(true));
            } else if (res.data.resultCode === 10) {
                const res = await authAPI.getCaptcha();

                dispatch(getCaptchaSuccess(res.data.url));
            } else {
                dispatch(toggleIsAuth(false));
                dispatch(setError(res.data.messages[0]));
            }
        } catch (error) {
            if (error instanceof AxiosError) {
                dispatch(setError(error.message));
            } else {
                dispatch(setError('Unexpected error...'));
            }
        }
    };
};

export const logout = (): AppThunkType => {
    return async dispatch => {
        try {
            const res = await authAPI.logout();

            if (res.data.resultCode === 0) {
                dispatch(toggleIsAuth(false));
                dispatch(setUserId(null));
                dispatch(setAvaLinkAC(null));
            } else {
                dispatch(setError(res.data.messages[0]));
            }
        } catch (error) {
            if (error instanceof AxiosError) {
                dispatch(setError(error.message));
            } else {
                dispatch(setError('Unexpected error...'));
            }
        }
    };
};

export const getCaptcha = (): AppThunkType => {
    return async dispatch => {
        try {
            const res = await authAPI.getCaptcha();

            dispatch(getCaptchaSuccess(res.data.url));
        } catch (error) {
            if (error instanceof AxiosError) {
                dispatch(setError(error.message));
            } else {
                dispatch(setError('Unexpected error...'));
            }
        }
    };
};
