import React, { ReactElement } from 'react';

import css from './Footer.module.scss';

export const Footer = (): ReactElement => {
    return <div className={css.footer} />;
};
